function fetchCustomers(page, size, salary, agesorting, desc){
    let pageNumber = (typeof page !== 'undefined') ?  page : 0;
    let sizeNumber = (typeof size !== 'undefined') ?  size : 5;
    let selectedsalary = (typeof salary !== 'undefined') ?  salary : -1;
    let ageSorted = (typeof agesorting !== 'undefined') ?  agesorting: false;
    let descDirection = (typeof desc !== 'undefined') ?  desc: false;

    /**
     * Do a fetching to get data from Backend's RESTAPI
     */
    $.ajax({
        type : "GET",
        url : "/securities/pageable",
        data: {
            page: pageNumber,
            size: sizeNumber,
            salary: selectedsalary,
            agesorting: ageSorted,
            desc: descDirection
        },
        success: function(response){
            $('#secTable tbody').empty();
            // add table rows
            $.each(response.securities, (i, secElement) => {
                let tr_id = 'tr_' + secElement.id;
            let secElementRow = '<tr>' +
                '<td>' + secElement.id + '</td>' +
                '<td>' + secElement.secid + '</td>' +
                '<td>' + secElement.shortname + '</td>' +
                '<td>' + secElement.regnumber + '</td>' +
                '<td>' + secElement.name + '</td>' +
                '</tr>';
            $('#secTable tbody').append(secElementRow);
        });

            if ($('ul.pagination li').length - 2 != response.totalPages){
                // build pagination list at the first time loading
                $('ul.pagination').empty();
                buildPagination(response.totalPages);
            }
        },
        error : function(e) {
            alert("ERROR: ", e);
            console.log("ERROR: ", e);
        }
    });
}