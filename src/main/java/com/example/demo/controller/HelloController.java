package com.example.demo.controller;

import com.example.demo.model.History;
import com.example.demo.model.SecElement;
import com.example.demo.model.Securities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.*;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.List;

import static javax.ws.rs.core.MediaType.APPLICATION_XML;
import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
public class HelloController {
    @Autowired
    private ApplicationContext ctx;

    final Logger logger = LoggerFactory.getLogger(HelloController.class);

    @RequestMapping(value = "/sec",produces = APPLICATION_XML)
    public Securities securities() throws JAXBException {
        Securities sec = ctx.getBean(Securities.class);
        return sec;
    }

    @RequestMapping(method = PUT, value = "/sec" ,consumes = APPLICATION_XML, produces = APPLICATION_XML)
    public Securities updateSec(@RequestBody Securities securities) {
        Securities sec = ctx.getBean(Securities.class);
        List<SecElement> elementList = sec.rows.elementList;
        List<SecElement> elementList1 = securities.rows.elementList;
        for (int i = 0; i < elementList.size(); i++) {
            for (int j = 0; j < elementList1.size(); j++) {
               if (elementList.get(i).getSecid().equals(elementList1.get(j).getSecid())) {
                   elementList.set(i,elementList1.get(j));
               }
            }
        }
        return sec;
    }

    @RequestMapping(method = DELETE, value = "/sec", consumes = APPLICATION_XML, produces = APPLICATION_XML)
    public Securities deleteSec(@RequestBody Securities securities) {
        Securities sec = ctx.getBean(Securities.class);
        List<SecElement> elementList = sec.rows.elementList;
        List<SecElement> elementList1 = securities.rows.elementList;
        for (int i = 0; i < elementList.size(); i++) {
            for (int j = 0; j < elementList1.size(); j++) {
                if (elementList.get(i).getSecid().equals(elementList1.get(j).getSecid())) {
                    elementList.remove(i);
                }
            }
        }
        return sec;
    }

    @RequestMapping(method = POST, value = "/sec", consumes = APPLICATION_XML, produces = APPLICATION_XML)
    public Securities createSec(@RequestBody Securities securities) {
        Securities sec = ctx.getBean(Securities.class);
        List<SecElement> elementList = sec.rows.elementList;
        List<SecElement> elementList1 = securities.rows.elementList;
        elementList.addAll(elementList1);
        return sec;
    }

    @RequestMapping(value = "/hist",produces = "application/xml")
    public History history() throws JAXBException {
        History hist = ctx.getBean(History.class);
        return hist;
    }

}
