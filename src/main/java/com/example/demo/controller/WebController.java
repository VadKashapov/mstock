package com.example.demo.controller;

import com.example.demo.model.SecElement;
import com.example.demo.model.Securities;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.List;
import java.util.regex.Pattern;

@Controller
public class WebController {
    @Autowired
    private ApplicationContext ctx;

    @GetMapping("/showAddSecurities")
    public String showAddSecuritiesForm(Model model) {
        model.addAttribute("secElement",new SecElement());
        return "securities-form";
    }

    @PostMapping("/addSecurities")
    public String showAddSecuritiesForm(@Valid SecElement secElement, BindingResult result, Model model) {
        String spt = (String) secElement.getName();
        if (spt.matches("[\\d\\s\\p{InCyrillic}]+")) {
            Securities sec = ctx.getBean(Securities.class);
            List<SecElement> elementList = sec.rows.elementList;
            for (int i = 0; i < elementList.size(); i++) {
                    if (elementList.get(i).getSecid().equals(secElement.getSecid())) {
                        elementList.set(i,secElement);
                        break;
                    }
                    elementList.add(secElement);
            }
        }
        return "securities";
    }

    @GetMapping("/securities")
    public String showSec(Model model) {
        model.addAttribute("securities",ctx.getBean(Securities.class));
        return "securities";
    }
}
