package com.example.demo;

import com.example.demo.model.SecElement;
import com.example.demo.model.Securities;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;

@SpringBootApplication
public class DemoApplication {
    public static void main(String[] args) throws JAXBException {
        SpringApplication.run(DemoApplication.class, args);
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(BeanConfigure.class);
    }
}
