package com.example.demo.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAttribute;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
public class HistoryElement implements Serializable {
    @XmlAttribute(name = "BOARDID")
    public String boardid;
    @XmlAttribute(name = "TRADEDATE")
    public Date tradedate;
    @XmlAttribute(name = "SHORTNAME")
    public String shortname;
    @XmlAttribute(name = "SECID")
    public String secid;
    @XmlAttribute(name = "NUMTRADES")
    public double numtrades;
    /*<columns>
<column name="BOARDID" type="string" bytes="12" max_size="0" />
<column name="TRADEDATE" type="date" bytes="10" max_size="0" />
<column name="SHORTNAME" type="string" bytes="189" max_size="0" />
<column name="SECID" type="string" bytes="36" max_size="0" />
<column name="NUMTRADES" type="double" />
<column name="VALUE" type="double" />
<column name="OPEN" type="double" />
<column name="LOW" type="double" />
<column name="HIGH" type="double" />
<column name="LEGALCLOSEPRICE" type="double" />
<column name="WAPRICE" type="double" />
<column name="CLOSE" type="double" />
<column name="VOLUME" type="double" />
<column name="MARKETPRICE2" type="double" />
<column name="MARKETPRICE3" type="double" />
<column name="ADMITTEDQUOTE" type="double" />
<column name="MP2VALTRD" type="double" />
<column name="MARKETPRICE3TRADESVALUE" type="double" />
<column name="ADMITTEDVALUE" type="double" />
<column name="WAVAL" type="double" />
</columns>*/
}
