package com.example.demo.model;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "document")
public class Securities {
   /* @XmlElement(name = "metadata")
    Metadata mdata;*/
    @XmlElement(name = "data")
   public Rows rows;
}
