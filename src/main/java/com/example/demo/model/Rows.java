package com.example.demo.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import java.util.List;

public class Rows {
    @XmlElementWrapper(name = "rows")
    @XmlElement(name = "row")
    public List<SecElement> elementList;
}
