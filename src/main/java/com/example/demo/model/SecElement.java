package com.example.demo.model;

import com.sun.xml.txw2.annotation.XmlElement;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Data
@NoArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name = "Securities")
public class SecElement implements Serializable {
    @XmlAttribute
    @Id
    private int id;
    @XmlAttribute
    @Column
    private String secid;
    @XmlAttribute
    @Column
    private String shortname;
    @XmlAttribute
    @Column
    private String regnumber;
    @XmlAttribute
    @Column
    private String name;

    /*public static SecElement getElemRandom() {
        return new SecElement(12, "sds", "sd", "sdd", "sdko");
    }*/
/*
<column name="id" type="int32" />
<column name="secid" type="string" bytes="36" max_size="0" />
<column name="shortname" type="string" bytes="189" max_size="0" />
<column name="regnumber" type="string" bytes="189" max_size="0" />
<column name="name" type="string" bytes="765" max_size="0" />
<column name="isin" type="string" bytes="765" max_size="0" />
<column name="is_traded" type="int32" />
<column name="emitent_id" type="int32" />
<column name="emitent_title" type="string" bytes="765" max_size="0" />
<column name="emitent_inn" type="string" bytes="30" max_size="0" />
<column name="emitent_okpo" type="string" bytes="24" max_size="0" />
<column name="gosreg" type="string" bytes="189" max_size="0" />
<column name="type" type="string" bytes="93" max_size="0" />
<column name="group" type="string" bytes="93" max_size="0" />
<column name="primary_boardid" type="string" bytes="12" max_size="0" />
<column name="marketprice_boardid" type="string" bytes="12" max_size="0" />
*/
}
