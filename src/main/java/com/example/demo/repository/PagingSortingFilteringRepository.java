package com.example.demo.repository;

import com.example.demo.model.SecElement;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Slice;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.awt.print.Pageable;

@NoRepositoryBean
public interface PagingSortingFilteringRepository<T,ID> extends CrudRepository<T,ID> {
    Iterable<T> findAll(Sort sort);
    Page<T> findAll(Pageable pageable);

    Slice findAllBySalary (double salary, Pageable pageable);
    Page findAllByAgeGreaterThan(int age, Pageable pageable);
}
