package com.example.demo;

import com.example.demo.model.History;
import com.example.demo.model.Securities;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.helpers.DefaultValidationEventHandler;
import java.io.File;

@Configuration
public class BeanConfigure {
    @Bean
    public History getHistory() throws JAXBException {
        File file = new File("history_1.xml");
        JAXBContext jaxbContext = JAXBContext.newInstance(History.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        //unmarshaller.setEventHandler(new DefaultValidationEventHandler());
        History history = (History) unmarshaller.unmarshal(file);
        System.out.println("sdso");
        return history;
    }

    @Bean
    public Securities getSecurities() throws JAXBException {
        File file = new File("securities_1.xml");
        JAXBContext jaxbContext = JAXBContext.newInstance(Securities.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        Securities securities = (Securities) unmarshaller.unmarshal(file);
        return securities;
    }
}
